import React, { Component } from 'react';
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import Toolbar from '@material-ui/core/Toolbar';
import {Grid, Col, Row} from 'react-flexbox-grid';
import LocationList from './components/LocationList';
import ForecastExtended from './components/ForecastExtended';

import './App.css';

const cities = [

  "Barcelona,es",
  "Paris,fr",
  "Mexico,mex",
  "Lima,pe",
]

class App extends Component {

  constructor() {
    super();
    this.state = {city: null};
  }


  handleSelectionLocation = city =>{
    this.setState({ city: city });
    console.log(`handleSelectionLocation ${city}`);
  };

  render() {
    const {city} = this.state; 
    return (
      <Grid>
        <Row>
          <AppBar position="sticky">
            <Toolbar>
              <Typography variant="title" color="inherit">Weather APP</Typography>
            </Toolbar>
          </AppBar>
        </Row>
        <Row className="bodyContainer">
          <Col xs={12} md={6} className="LocationListContainer">
            <LocationList 
              cities={cities} 
              onSelectedLocation={this.handleSelectionLocation}
            />
          </Col>
          <Col xs={12} md={6} className="detailsContainer">
            <Paper zdepth={4}>
              <div className="details">
                { !city  ?
                  null : 
                  <ForecastExtended city={city} ></ForecastExtended>
                }
              </div>
            </Paper>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default App;
