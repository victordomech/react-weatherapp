import React, {Component} from 'react';
import {PropTypes} from 'prop-types';
import {CircularProgress} from '@material-ui/core';

import Location from './Location';
import WeatherData from './WeatherData';
import transformWeather from './../../services/transformWeather';
import getUrlByCity from './../../services/getUrlByCity';

import './style.css';


class WeatherLocation extends Component{

    constructor(props) {

        super(props);
        const { city } = props;

        this.state = {
            city,
            data: null,
        };
    }

    componentDidMount() {
        
        this.handleUpdateClick();
    }
    
    componentDidUpdate(prevProps, prepState){

    }

    handleUpdateClick = () =>{
        const api_weather = getUrlByCity(this.state.city);
        fetch(api_weather).then( resolve => {

            return resolve.json();
        }).then( data =>{
            const newWeather = transformWeather(data);

            this.setState({

                data: newWeather
            });
        });
    }
    render(){
        const {onWeatherLocationClick} = this.props;
        const {city, data} = this.state;
        return (
            <div className="weatherLocation_container" onClick={onWeatherLocationClick}>
                <Location city={city} />
                {data ? <WeatherData  data={data}/> : <CircularProgress />}
            </div>
        )
    }
}

WeatherLocation.propTypes = {

    city: PropTypes.string.isRequired,
    onWeatherLocationClick:PropTypes.func.isRequired,
};

export default WeatherLocation;